package com.lagou.test;

import com.lagou.domain.Article;
import com.lagou.mapper.ArticleMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ArticleTest {


    @Test
    public void test2() throws IOException {

        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);

        SqlSession sqlSession = sqlSessionFactory.openSession();

        ArticleMapper articleMapper = sqlSession.getMapper(ArticleMapper.class);

        List<Article> allComment = articleMapper.findAllComment();

        for (Article article : allComment) {
            System.out.println(article);


            System.out.println(article.getCommentList());
        }

        sqlSession.close();


    }

}
