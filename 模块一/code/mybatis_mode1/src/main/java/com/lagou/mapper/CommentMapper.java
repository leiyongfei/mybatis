package com.lagou.mapper;

import com.lagou.domain.Comment;

import java.util.List;

public interface CommentMapper {

    /**
     * 根据 aid 查询评论信息
     * @param aid
     * @return
     */
    public List<Comment> findByAid(Integer aid);
}
