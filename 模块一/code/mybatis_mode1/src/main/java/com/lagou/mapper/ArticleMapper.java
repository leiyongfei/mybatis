package com.lagou.mapper;

import com.lagou.domain.Article;

import java.util.List;

public interface ArticleMapper {


    /**
     * 查询内容
     */
    public List<Article> findAllComment();
}
